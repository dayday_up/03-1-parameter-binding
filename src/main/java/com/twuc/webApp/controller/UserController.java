package com.twuc.webApp.controller;

import com.twuc.webApp.model.DateTime;
import com.twuc.webApp.model.Person;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {
    @GetMapping("/user/{userId}")
    private String getUser(@PathVariable Integer userId) {
        return "userId:" + userId;
    }

    @GetMapping("/user/{userId}/books")
    private String getUser(@PathVariable int userId) {
        return "userId:" + userId;
    }

    @GetMapping("/user/{userId}/book/{bookId}")
    private String getUser(@PathVariable("userId") Integer userId, @PathVariable("bookId") Integer bookId) {
        return "userId:" + userId + "," + "bookId:" + bookId;
    }

    @GetMapping("/users")
    private String queryUserByAge(@RequestParam int age) {
        return "age:" + age;
    }

    @GetMapping("/users/collection")
    private List<String> getUserCollection(@RequestParam List<String> params) {
        return params;
    }

    @PostMapping("/datetimes")
    private ZonedDateTime getDate(@RequestBody DateTime date) {
        return date.getDateTime();
    }

    @GetMapping("/valid")
    private Person createPerson(@Valid @RequestBody Person person) {
        return person;
    }
}
