package com.twuc.webApp.model;

import javax.validation.constraints.*;

public class Person {
    public Person() {}
    public Person(String name, Integer yearOfBirth, String email) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    @NotNull
    @Size(min=2, max=30)
    private String name;

    @Min(1000)
    @Max(9999)
    private Integer yearOfBirth;

    @Email
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(Integer yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
