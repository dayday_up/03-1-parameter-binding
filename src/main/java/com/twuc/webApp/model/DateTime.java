package com.twuc.webApp.model;

import java.time.ZonedDateTime;
import java.util.Date;

public class DateTime {
    private ZonedDateTime dateTime;

    public DateTime() {}

    public DateTime(ZonedDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(ZonedDateTime dataTime) {
        this.dateTime = dataTime;
    }
}
