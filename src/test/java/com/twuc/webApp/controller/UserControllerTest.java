package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.model.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper mapper = new ObjectMapper();

    @Test
    void should_return_200_when_bind_integer_type() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user/3"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("userId:3"));
    }

    @Test
    void should_return_5xx_server_error_when_bind_int_primitive_type() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user/1/books"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void should_return_200_when_bind_more_than_one_path_variable() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.get("/api/user/1/book/123")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("userId:1,bookId:123"));
    }

    @Test
    void should_return_200_and_return_correct_value_when_bind_request_variable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users?age=18"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("age:18"));
    }

    @Test
    void should_return_500_status_when_bind_request_variable_but_not_send() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should_return_collection_when_sent_several_same_request_variable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/collection")
                .param("params", "1", "2", "3"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[\"1\",\"2\",\"3\"]"));
    }

    @Test
    void should_return_serialized_string_of_student() throws JsonProcessingException {
        Student student = new Student("lisa", 18);
        assertEquals("{\"name\":\"lisa\",\"age\":18}", mapper.writeValueAsString(student));
    }

    @Test
    void should_return_deserialize_object_of_student() throws IOException {
        String json = "{\"name\":\"jack\",\"age\":23}";
        Student student = new Student("jack", 23);
        student.equals(mapper.readValue(json, Student.class));
    }

    /*

     */

    @Test
    void should_return_200_and_status_itself_when_sent_date_object() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect((MockMvcResultMatchers.content().string("\"2019-10-01T10:00:00Z\"")));
    }

    @Test
    void should_return_400_status_when_sent_date_is_not_ISO() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
        .content("{ \"dateTime\": \"2019-10-01\" }")
        .contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should_return_400_status_when_not_given_name() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/valid")
                .content("{}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void should_return_200_status_when_given_name() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/valid")
                .content("{\"name\": \"aaaaa\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
